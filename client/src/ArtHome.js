import React from 'react';
import './App.css';
import AppNav from "./AppNav";
import { getWeb3, getInstance}  from "./Web3Utils";
import Swal from 'sweetalert2';

/**
 */
class ArtHome extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      hasData: false,
      message: "",
      rows:[],
      columns: [],
      user: '',
      tokenIds: [],
      title: [],
      desc: [],
      price: [],
      publishDate: [],
      author: [],
      authorAdrress:[],
      image: [],
      total: 0,
      contractInstance: '',
      alert:false,
      showBuy:true,
      tokenAddress:[],
    };
    this.buyArt = this.buyArt.bind(this);
  }
  resetPendingArts() {
    this.setState({ 
      tokenIds: [],
      title: [],
      desc: [],
      price: [],
      publishDate: [],
      author: [],
      image: [],
      total: 0
    });
  }
  componentDidMount = async () => {
    const web3 = await getWeb3();
    window.web3 = web3;
    const contractInstance = await getInstance(web3);
    window.user = (await web3.eth.getAccounts())[0];
    this.setState({ user: window.user });
    this.setState({ contractInstance: contractInstance });
    await this.loadDigitalArts(web3);

  }

  async loadDigitalArts(web3) {
      try {
        let ids;
        const result = await this.state.contractInstance.methods.findAllPendingArt().call();
        ids = result[0];
        let _total = ids.length;
        if(ids && _total>0) {
          let row;
          if(_total<=3) {
            row = 1;
          } else {
            row = Math.ceil(_total/3);
          }
          let columns = 3;
          this.setState({ rows: [], columns: [] });
          let rowArr = Array.apply(null, {length: row}).map(Number.call, Number);
          let colArr = Array.apply(null, {length: columns}).map(Number.call, Number);
          this.setState({ rows: rowArr, columns: colArr });
          let _tokenIds= [], _title =[],  _desc= [], _price= [], _publishDate= [], _author=[], _author_address=[], _image =[];
          let _tokenOwnerAddres = [];
          let idx =0;
          this.resetPendingArts();
          for(let i = 0; i<row; i++) {
            for(let j = 0; j < columns; j++) {
               if(idx<_total) {
                 let tokenId= ids[idx];
                 const art = await this.state.contractInstance.methods.findArt(tokenId).call();
                 const tokenOfAddress = await this.state.contractInstance.methods.ownerOf(tokenId).call();
                 if(tokenOfAddress === this.state.user){
                    _tokenOwnerAddres.push(tokenOfAddress);
                 } 
                 const priceInEther = web3.utils.fromWei(art[3], 'ether');
                 _tokenIds.push(art[0]);
                 _title.push(art[1]);
                 _desc.push(art[2]);
                 _price.push(priceInEther);
                 _publishDate.push(art[4]);
                 _image.push(art[9]);
                 _author.push(art[5]);
                 _author_address.push(art[6]);
               }
              idx++;
            }
        }
         
          this.setState({ 
            tokenIds: _tokenIds,
            title: _title,
            desc: _desc,
            price: _price,
            publishDate: _publishDate,
            author: _author,
            authorAdrress:_author_address,
            image: _image,
            total: _total,
            tokenAddress: _tokenOwnerAddres,
          });
          this.setState({ hasData: true });
        } else {
          this.setState({ hasData: false });
        }
 
    } catch (e) {console.log('Error', e)}
  
  }
  buyArt(tokenId, priceInEther) {
    const pricecommision =  window.web3.utils.fromWei('1000000000000000000', 'ether');
    const totalPay = Number(pricecommision)+Number(priceInEther)
    Swal.fire({
      title: 'Are you sure?',
      text: "The transactions are irreversible!. A commission of 0.0005 ether will be charged. Your total to pay will be "+`${totalPay}`+" ether ",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, buy!'
    }).then((result) => {
      if (result.isConfirmed) {
        this.transaxion(tokenId,priceInEther,pricecommision);
        
      }
    })
  };

  async transaxion(tokenId,priceInEther,pricecommision){
      try {
        const priceInWei =  window.web3.utils.toWei(priceInEther, 'ether');
        const priceInWeii =  window.web3.utils.toWei(pricecommision, 'ether');
        const total = Number(priceInWei)+Number(priceInWeii)
         await this.state.contractInstance.methods.buyArt(tokenId).send({
            from: this.state.user, gas: 6000000, value: total
        })
       window.location.reload(); 
      } catch (e) {console.log('Error', e)}
    }


  render() {
    if (this.state.hasData) {
      return (
        <div className="App">
          <AppNav></AppNav>
          <section className="text-center" style={{ marginTop: '0px'}}>
          
            <h4 className="h4-responsive font-weight-bold text-center my-5 text-light">Buy/Sell Digital Art on our Art Gallery</h4>
            <div className="container">
               {this.state.rows.map((row, i) =>
                <div className="row" key={i}>
                  {this.state.columns.map((col, j) =>
                    <div className="col-lg-4 col-md-12 mb-lg-0 mb-0" key={j}>
                        { i*3+j < this.state.total &&
                            <div >
                            <div className="view overlay rounded z-depth-3 mb-2">
                              <img className="img-fluid" src={this.state.image[i*3+j]} alt="Sample"/>
                            </div>
                            <h5 className="pink-text font-weight-bold mb-1 text-light"><i class="fab fa-ethereum"></i></h5>
                            <div className="font-weight-bold orange-text deep-orange-lighter-hover text-light">TokenId: {this.state.tokenIds[i*3+j]}</div>
                            <h5 className="font-weight-bold mb-1 text-light">Title: {this.state.title[i*3+j]}</h5>
                            <div className="dark-grey-text text-light">{this.state.price[i*3+j]} (ether)</div>
                            <p className="text-light">by <span className="font-weight-bold text-light">{this.state.author[i*3+j]}</span>, {this.state.publishDate[i*3+j]}</p>
                            
                            <p className="font-weight-bold mb-1 text-white bg-dark">{this.state.desc[i*3+j]}</p>
                            <br></br>
                              {this.state.tokenAddress[i*3+j] !== this.state.user &&
                                 <button className="btn btn-white btn-rounded btn-md" onClick={e => (e.preventDefault(),this.buyArt(this.state.tokenIds[i*3+j], this.state.price[i*3+j]))}>Buy</button>                          
                              }  
                           </div>
                        }
                    </div>

                  )}
                </div>
              )}
              </div>
          </section>
        </div>
      );
    } else {
      return (
        <div className="App"> 
          <AppNav></AppNav>
          <section className="text-center">
            <h5 className="h1-responsive font-weight-bold text-center my-5">Buy Digital Art on our Art Gallery</h5>
            <div className="container">
              <div className="alert alert-primary" role="alert">
                Publish your digital arts in blockchain today!
              </div>
              </div>
          </section>
        </div>
      );
    }

  }
}

export default ArtHome;
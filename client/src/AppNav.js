import React, { Component } from 'react';
import { withRouter} from 'react-router-dom';
import "./App.css";
import { getWeb3, getInstance}  from "./Web3Utils";


class AppNav extends React.Component {
    constructor(props) {
      super(props);
      this.handleNavClick = this.handleNavClick.bind(this);
      this.state = { 
            name: '',
            symbol: ''
        };
    }
    handleNavClick = param => e => {
      e.preventDefault();
      this.props.history.push('/'+param)
    };
    componentDidMount = async () => {

      const web3 = await getWeb3();
      const contractInstance = await getInstance(web3);
      window.user = (await web3.eth.getAccounts())[0];
      const symbol = await contractInstance.methods.symbol().call()
      this.setState({ symbol: symbol });
            const name = await contractInstance.methods.name().call();
      this.setState({ name: name });
  }

  render() {
    return (       
<nav className="navbar fixed-top navbar-expand-lg navbar-dark scrolling-navbar text-light bg-dark">

  <div className="container-fluid ">

    <button
      className="navbar-toggler"
      type="button"
      data-mdb-toggle="collapse"
      data-mdb-target="#navbarSupportedContent"
      aria-controls="navbarSupportedContent"
      aria-expanded="false"
      aria-label="Toggle navigation"
    >
    </button>

  
    <div className="collapse navbar-collapse justify-content-start" id="navbarSupportedContent">
    
      <ul className="navbar-nav me-auto mb-2 mb-lg-0">
        <li className="nav-item">
        <a className="btn btn-outline-white btn-md my-2 my-sm-0 ml-3" href="/home">Art Gallery</a>
        </li>
        <li className="nav-item">
        <a className="btn btn-outline-white btn-md my-2 my-sm-0 ml-3" href="/publishArt">Publish Art</a>
        </li>
        <li className="nav-item">
        <a className="btn btn-outline-white btn-md my-2 my-sm-0 ml-3" href="/myWallet">My Wallet Info</a>
        </li>
      </ul>
    </div>

    <div className="d-flex navbar-collapse flex-row d-flex d-md-flex text-white"><h1>Decentralized Token Market</h1>
    </div>

    <div className="d-flex align-items-end">

      <a className="text-reset me-3" href="#">
      <i className="fa fa-coins fa-shopping-cart text-white"></i>{" Symbol " + this.state.symbol} 
      </a>
    
      <a
        className="dropdown-toggle d-flex align-items-center hidden-arrow "
        href="#"
        id="navbarDropdownMenuLink"
        role="button"
        data-mdb-toggle="dropdown"
        aria-expanded="false"
      >
        <img
          src="images/ether.png"
          class="rounded-circle"
          height="34"
          alt=""
          loading="lazy"
        />
      </a>
      <ul
        className="dropdown-menu dropdown-menu-end"
        aria-labelledby="navbarDropdownMenuLink"
      >
        <li>
          <a className="dropdown-item" href="/home">Art Gallery</a>
        </li>
        <li>
          <a className="dropdown-item" href="/publishArt">Publish Art</a>
        </li>
        <li>
          <a className="dropdown-item" href="/myWallet">My Wallet Info</a>
        </li>
      </ul>
    </div>
  
  </div>
  
</nav>
    );
  }
}

export default withRouter(AppNav);

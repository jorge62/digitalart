import React from 'react';
import './App.css';
import AppNav from "./AppNav";
import { getWeb3, getInstance}  from "./Web3Utils";
import Swal from 'sweetalert2';


/**
 */
class MyWallet extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      hasData: false,
      message: "",
      rows:[],
      columns: [],
      tokenIds: [],
      title: [],
      desc: [],
      price: [],
      publishDate: [],
      author: [],
      authorAdrress:[],
      image: [],
      status:[],
      total: 0,
      user: '',
      balance: 0,
      contractInstance: '',
      networkId:'',
      networkType:'',
      sellTokenId: '',
      sellPrice:0,
      showModal: false
    };
    this.changeHandler = this.changeHandler.bind(this);
  }
  resetPendingArts() {
    this.setState({ 
      tokenIds: [],
      title: [],
      desc: [],
      price: [],
      publishDate: [],
      author: [],
      image: [],
      status:[],
      total: 0
    });
  }
  componentDidMount = async () => {
    const web3 = await getWeb3();
    window.web3 = web3;
    const contractInstance = await getInstance(web3);
    window.user = (await web3.eth.getAccounts())[0];
    const balanceInWei = await web3.eth.getBalance(window.user);
    var balance = web3.utils.fromWei(balanceInWei, 'ether');
    const networkId = await web3.eth.net.getId();
    const networkType = await web3.eth.net.getNetworkType();
    this.setState({ user: window.user });
    this.setState({ balance: balance});
    this.setState({ contractInstance: contractInstance });
    this.setState({ networkId: networkId});
    this.setState({ networkType: networkType});
    await this.loadMyDigitalArts(web3);

  }
  async loadMyDigitalArts(web3) {
      try {
        let ids;
        const result = await this.state.contractInstance.methods.findMyArts().call();
        let _total = result.length;
        if(_total>0) {
          let row;
          if(_total<=3) {
            row = 1;
          } else {
            row = Math.ceil(_total/3);
          }
          let columns = 3;
          let rowArr = Array.apply(null, {length: row}).map(Number.call, Number);
          let colArr = Array.apply(null, {length: columns}).map(Number.call, Number);
          this.setState({ rows: rowArr, columns: colArr });
          let _tokenIds= [], _title =[],  _desc= [], _price= [], _publishDate= [],  _author=[], _author_address=[], _image =[], _status=[];
          let idx =0;
          this.resetPendingArts();
          for(let i = 0; i<row; i++) {
            for(let j = 0; j < columns; j++) {
               if(idx<_total) {
                 let tokenId= result[idx];
                 const art = await this.state.contractInstance.methods.findArt(tokenId).call();
                 const priceInEther = web3.utils.fromWei(art[3], 'ether');
                 _tokenIds.push(art[0]);
                 _title.push(art[1]);
                 _desc.push(art[2]);
                 if(art[8]==1) {
                    _status.push("In selling");
                 } else {
                    _status.push("Publish");
                 }
                
                 _price.push(priceInEther);
                 _publishDate.push(art[4]);
                 _author.push(art[5]);
                 _author_address.push(art[6]);
                 _image.push(art[9]);
               }
              idx++;
            }
        }
         
          this.setState({ 
            tokenIds: _tokenIds,
            title: _title,
            desc: _desc,
            price: _price,
            publishDate: _publishDate,
            author: _author,
            authorAdrress:_author_address,
            status: _status,
            image: _image,
            total: _total
          });
          this.setState({ hasData: true });
        } else {
          this.setState({ hasData: false });
        }
 
    } catch (e) {console.log('Error', e)}
  
  }
  changeHandler = event => {
    this.setState({
        [event.target.name]: event.target.value
        }, function(){ })
   };

   sellArt(tokenId) {
    try {
      //open window for resell
      this.setState({ sellTokenId: tokenId, showModal: true });
      Swal.fire({
        title: "Sell NFT",
        text: "Write the price of sell: ",
        input: 'text',
        showCancelButton: true        
      }).then((result) => {
        if (result.value) {
            this.setState({sellPrice:result.value});
            console.log(this.state.sellPrice)
            this.submitArtSell();
        }
      });
    } catch (e) {console.log('Error', e)}
  };

  async submitArtSell() {
    try {
      const priceInWei =  window.web3.utils.toWei(this.state.sellPrice, 'ether');
       await this.state.contractInstance.methods.resellArt(this.state.sellTokenId, priceInWei).send({
           from: this.state.user, gas: 6000000
       })
      window.location.reload(); 
    } catch (e) {console.log('Error', e)}
  };


  render() {
    if (this.state.hasData) {
      return (
        <div className="App">
          <AppNav></AppNav>
          <section className="text-center" >
            <div className="row mb-2 mt-2">
                    <div className="col-md-2 mb-md-0 mb-1"></div>
                    <div className="col-md-12 mb-md-0 mb-1">
                        <div className="card">
                            <div className="card-body ">
                            <h3 className="h3-responsive font-weight-bold text-white bg-dark">My Wallet Arts</h3>
                            <br></br>
                              <div className="row">
                                <div className="col-md-6 mb-md-0">
                                    <span className="font-weight-bold blue-grey-text text-light">My Address:</span> <span className ="text-light">{this.state.user}</span>
                                </div>
                                <div className="col-md-6 mb-md-0">
                                    <span className="font-weight-bold blue-grey-text text-light">NetworkId:</span><span className ="text-light">{this.state.networkId}</span>
                                </div>
                              </div>
                              <div className="row">
                                <div className="col-md-6 mb-md-0">
                                    <span className="font-weight-bold blue-grey-text text-light">{'Balance: '+this.state.balance} ( ether )</span>
                                </div>
                                <div className="col-md-6 mb-md-0">
                                    <span className="font-weight-bold blue-grey-text text-light">NetworkType:</span><span className ="text-light">{this.state.networkType}</span>
                                </div>
                              </div>
                            </div>
                        </div>
                    </div>
            </div>
            <div className="container">
               {this.state.rows.map((row, i) =>
                <div className="row" key={i}>
                  {this.state.columns.map((col, j) =>
                    <div className="col-lg-4 col-md-12 mb-lg-0 mb-0" key={j}>
                        { i*3+j < this.state.total &&
                            <div>
                            <div className="view overlay rounded z-depth-3 mb-2">
                              <img className="img-fluid" src={this.state.image[i*3+j]} alt="Sample"/>
                            </div>
                            <h5 className="pink-text font-weight-bold mb-1 text-light"><i className="fab fa-ethereum"></i></h5>
                            <div className="font-weight-bold orange-text deep-orange-lighter-hover text-light">TokenId: {this.state.tokenIds[i*3+j]}</div>
                            <h5 className="font-weight-bold mb-1 text-light">Title: {this.state.title[i*3+j]}</h5>
                            <div className="dark-grey-text text-light">{this.state.price[i*3+j]} (ether)</div>
                            <p className="text-light">by  <span className="font-weight-bold text-light">{this.state.author[i*3+j]}</span>(<span className="font-weight-bold text-light">{this.state.authorAdrress[i*3+j]})</span></p>
                            <p className="text-light">Date Creation  <span className="font-weight-bold text-light">{this.state.publishDate[i*3+j]}</span></p>                            
                            <h5 className="font-weight-bold mb-1 text-white bg-dark">{this.state.desc[i*3+j]}</h5>
                            { this.state.status[i*3+j]==='Publish' &&
                                <button className="btn btn-pink btn-rounded btn-md text-white" data-toggle="modal" data-target="#exampleModal" onClick={e => (e.preventDefault(),this.sellArt(this.state.tokenIds[i*3+j]))} data-target=".sell-modal" ><span className="text-white">{this.state.status[i*3+j]}</span></button>
                            }
                           
                        </div>
                        }
                    </div>

                  )}
                </div>
              )}
              </div>
          </section>
        </div>
      );
    } else {
      return (
        <div className="App">
          <AppNav></AppNav>
          <section className="text-center">
            <div className="row mb-2 mt-2">
                    <div className="col-md-2 mb-md-0 mb-1"></div>
                    <div className="col-md-12 mb-md-0 mb-1">
                        <div className="card">
                            <div className="card-body ">
                            <h3 className="h3-responsive font-weight-bold text-white bg-dark">My Wallet Arts</h3>
                            <br></br>
                              <div className="row">
                                <div className="col-md-6 mb-md-0">
                                    <span className="font-weight-bold blue-grey-text text-light">My Address:</span> <span className ="text-light">{this.state.user}</span>
                                </div>
                                <div className="col-md-6 mb-md-0">
                                    <span className="font-weight-bold blue-grey-text text-light">NetworkId:</span><span className ="text-light">{this.state.networkId}</span>
                                </div>
                              </div>
                              <div className="row">
                                <div className="col-md-6 mb-md-0">
                                    <span className="font-weight-bold blue-grey-text text-light">{'Balance: '+this.state.balance} ( ether )</span>
                                </div>
                                <div className="col-md-6 mb-md-0">
                                    <span className="font-weight-bold blue-grey-text text-light">NetworkType:</span><span className ="text-light">{this.state.networkType}</span>
                                </div>
                              </div>
                            </div>
                        </div>
                    </div>
            </div>
            <div className="col-md-2 mb-md-0 mb-1"></div>
        </section>
        </div>
      );
    }

  }
}

export default MyWallet;